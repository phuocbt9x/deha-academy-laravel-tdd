@extends('layouts.task.main')
@section('contents')
    <div class="container p-2">
        <h1>Tasks</h1>
        <a href="{{ route('task.index') }}" class="btn btn-success">Tasks List</a>
    </div>

    <div class="container p-5 my-5 border">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 10%">Id</th>
                <th style="width: 20%">Name</th>
                <th style="width: 50%">Contents</th>
                <th style="width: 20%">Actions</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        {{ $task->id }}
                    </td>
                    <td>
                        {{ $task->name }}
                    </td>
                    <td>
                        {{ $task->contents }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
