@extends('layouts.task.main')
@section('contents')
    <div class="container p-2">
        <h1>Tasks</h1>
        <a href="{{ route('task.create') }}" class="btn btn-success">Create Task</a>
    </div>

    <div class="container p-5 my-5 border">
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width: 10%">Id</th>
                <th style="width: 20%">Name</th>
                <th style="width: 50%">Contents</th>
                <th style="width: 20%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tasks as $task)
                <tr>
                    <td>
                        {{ $task->id }}
                    </td>
                    <td>
                        {{ $task->name }}
                    </td>
                    <td>
                        {{ $task->contents }}
                    </td>
                    <td style="text-align: center">
                        <div class="d-flex justify-content-around">
                            <a href="{{ route('task.show', $task->id) }}" class="btn btn-sm btn-info">
                                <i class="fas fa-eye"></i>
                            </a>
                            <a href="{{ route('task.edit', $task->id) }}" class="btn btn-sm btn-secondary">
                                <i class="fas fa-edit"></i>
                            </a>
                            <form action="{{ route('task.delete', $task->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger">
                                    <i class="fas fa-trash "></i>
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $tasks->links() }}
    </div>
@endsection
