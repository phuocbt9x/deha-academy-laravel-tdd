<!DOCTYPE html>
<html lang="en">
@include('layouts.task.header')
<body>

@yield('contents')

@include('layouts.task.footer')

</body>
</html>
