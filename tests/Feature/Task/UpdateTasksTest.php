<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTasksTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_see_view_update_task_form(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->id;
        $response = $this->get($this->getRouteTaskEdit($task));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('task.update');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_view_update_task_form(){
        $task = Task::factory()->create()->id;
        $response = $this->get($this->getRouteTaskEdit($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_update_task_if_data_validated(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->id;
        $data = Task::factory()->make()->toArray();
        $response = $this->put($this->getRouteTaskUpdate($task), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteTaskIndex());
        $this->assertDatabaseHas('tasks', $data);
    }

    /** @test */
    public function unauthenticated_user_can_not_update_task_if_data_validated(){
        $task = Task::factory()->create()->id;
        $data = Task::factory()->make()->toArray();
        $response = $this->put($this->getRouteTaskUpdate($task), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_data_not_validated(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->id;
        $data = Task::factory()->make(['name' => null])->toArray();
        $response = $this->put($this->getRouteTaskUpdate($task), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_see_data_message_error_if_data_not_validated(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->id;
        $data = Task::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->getRouteTaskEdit($task))->put($this->getRouteTaskUpdate($task), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteTaskEdit($task));
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_task_not_exits(){
        $this->actingAs(User::factory()->create());
        $data = Task::factory()->make()->toArray();
        $response = $this->put($this->getRouteTaskUpdate(-1), $data);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function getRouteTaskEdit($id){
        return route('task.edit', $id);
    }

    public function getRouteTaskUpdate($id){
        return route('task.update', $id);
    }

    public function getRouteTaskIndex(){
        return route('task.index');
    }

    public function getRouteLogin(){
        return route('login');
    }
}
