<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateTasksTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_see_view_create_task_form(){
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getRouteTaskCreate());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('task.create');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_view_create_task_form(){
        $response = $this->get($this->getRouteTaskCreate());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_create_task(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getRouteTaskStore(), $task);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteTaskIndex());
        $this->assertDatabaseHas('tasks', $task);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_task(){
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getRouteTaskStore(), $task);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_create_task_if_data_not_validated(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->getRouteTaskStore(), $task);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }


    /** @test */
    public function authenticate_user_can_see_data_message_error_if_data_not_validated(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->getRouteTaskCreate())->post($this->getRouteTaskStore(), $task);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteTaskCreate());
    }

    public function getRouteLogin(){
        return route('login');
    }

    public function getRouteTaskCreate(){
        return route('task.create');
    }

    public function getRouteTaskStore(){
        return route('task.store');
    }

    public function getRouteTaskIndex(){
        return route('task.index');
    }
}
