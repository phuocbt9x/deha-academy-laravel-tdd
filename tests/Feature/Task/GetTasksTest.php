<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetTasksTest extends TestCase
{
    /** @test */
    public function user_can_get_list_tasks(){
        $task = Task::factory()->create();
        $response = $this->get($this->getRouteTaskIndex());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('task.index');
        $response->assertSee($task->name)
                ->assertSee($task->contents);
    }

    public function getRouteTaskIndex(){
        return route('task.index');
    }
}
