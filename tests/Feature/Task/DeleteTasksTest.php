<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTasksTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_delete_task_if_task_exits(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->id;
        $response = $this->delete($this->getRouteTaskDelete($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteTaskIndex());
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_task_if_task_exits(){
        $task = Task::factory()->create()->id;
        $response = $this->delete($this->getRouteTaskDelete($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_delete_task_if_task_not_exits(){
        $this->actingAs(User::factory()->create());
        $response = $this->delete($this->getRouteTaskDelete(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function getRouteTaskIndex(){
        return route('task.index');
    }
    public function getRouteTaskDelete($id){
        return route('task.delete', $id);
    }
    public function getRouteLogin(){
        return route('login');
    }
}
