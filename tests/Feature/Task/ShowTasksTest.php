<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowTasksTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_see_task_if_task_exist(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get($this->getRouteTaskShow($task->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('task.show');
        $response->assertSee($task->name)
                ->assertSee($task->contents);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_task_if_task_exist(){
        $task = Task::factory()->create();
        $response = $this->get($this->getRouteTaskShow($task->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRouteLogin());
    }

    /** @test */
    public function authenticate_user_can_not_see_task_if_task_not_exist(){
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getRouteTaskShow(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function getRouteTaskShow($id){
        return route('task.show', $id);
    }
    public function getRouteLogin(){
        return route('login');
    }
}
